<?php

$www_root = '/home/www/';
$project_name = '';

/* security */
$access_token = '';

//ip地址为gitlab服务器请求地址
//$access_ip = array({server});
$allow_ip = [];


//查询服务器运行的php-fpm用户和文件所属权限是否一致
//print_r($_SERVER);

//文件记录日志
/* create open log */
$log_file = './logs/' . date('Y-m-d') . '.log';
$fs = fopen($log_file, 'a');
fwrite($fs, 'Request on ['.date("Y-m-d H:i:s").'] from ['.$request_ip.']'.PHP_EOL);

//token 检测
if ($access_token) {
    /* get user token and ip address */
    //$request_token = $_REQUEST['token'];
    $request_token = $_SERVER['HTTP_X_GITLAB_TOKEN'];

    if($request_token !== $access_token){
        echo "error 403";
        fwrite($fs, "Invalid token [{$request_ip}]".PHP_EOL);
        exit(0);
    }
}

//ip检测
if ($allow_ip) {
    $request_ip = $_SERVER['REMOTE_ADDR'];
    if(!in_array($request_ip, $allow_ip)){
        echo "error 503";
        fwrite($fs, "Invalid ip [{$request_ip}]".PHP_EOL);
        exit(0);
    }
}

//git push 时触发的json数据，可参考gitlab中web_hooks介绍
/* get json data */
$php_input = file_get_contents('php://input');
$php_input = json_decode($php_input, true);

$project_name = $php_input['project']['name'];

/* get branch */
$branch = $php_input["ref"];
fwrite($fs, '======================================================================='.PHP_EOL);
/* if you need get full json input */
//fwrite($fs, 'DATA: '.print_r($data, true).PHP_EOL);

/* branch filter */
if ($branch === 'refs/heads/master'){// todo 带完善
    /* if master branch*/
//    fwrite($fs, 'BRANCH: '.print_r($branch, true).PHP_EOL);
//    fwrite($fs, '======================================================================='.PHP_EOL);
//    $fs and fclose($fs);
    /* then pull master */

//    system("cd {projects} && git checkout master");
//    system("git pull");

//} elseif($branch === 'refs/heads/develop' && $php_input['object_kind']) {
} elseif($branch === 'refs/heads/develop' && $php_input['event_name'] == 'push') {

    /* if devel branch */
    fwrite($fs, 'BRANCH: '.print_r($branch, true).PHP_EOL);
    fwrite($fs, '======================================================================='.PHP_EOL);

    $project_path = $www_root . $project_name;

    fwrite($fs, $project_path.PHP_EOL);


    if(!is_dir($project_path)){
        mkdir($project_path, 0755);

        $git_ssh_url = $php_input['project']['git_ssh_url'];

        /* checkout and change branch to develop */
        $system_flag = system(
            "cd {$project_path}"
            . "&& git clone {$git_ssh_url}"
            . "&& git checkout -b develop origin/develop"//origin/develop 指定本地和远程分支对应关系，不然 pull 会出问题
        );
    }else{//目前主要用这里
        /* pull devel branch */
        $system_flag = system(
            "cd {$project_path}"
            . "&& sudo -u www git pull"
        );
    }
}

fwrite($fs, $system_flag . PHP_EOL);

$fs and fclose($fs);
